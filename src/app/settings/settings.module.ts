import { NgModule } from '@angular/core';
import { SettingsOverviewComponent } from './overview/settings-overview.component';

@NgModule({
    imports: [
    ],
    declarations: [
        SettingsOverviewComponent
    ],
    exports: []
})
export class SettingsModule {}