import { Component } from '@angular/core';

@Component({
    templateUrl: 'settings-overview.component.html',
    styleUrls: ['../settings.less']
})
export class SettingsOverviewComponent {
    

    constructor(
        
    ) {
        
    }

    ngOnInit() {
        
    }

    navigate = function(route: string){
        console.log("Nav");
        this.router.navigate([route]);
    }
}