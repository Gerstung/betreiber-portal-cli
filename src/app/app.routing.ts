﻿import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home';
import { LoginComponent } from './login';
import { RegisterComponent } from './register';
import { AuthGuard } from './_guards';
import { CalendarsComponent } from './calendars';
import { AddCalendarComponent } from './addCalendar';
import { SettingsOverviewComponent } from './settings';

const appRoutes: Routes = [
    { path: '', component: HomeComponent, canActivate: [AuthGuard] },
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'calendars', component: CalendarsComponent },
    { path: 'addCalendar', component: AddCalendarComponent },
    { path: 'settings', component: SettingsOverviewComponent},

    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes);