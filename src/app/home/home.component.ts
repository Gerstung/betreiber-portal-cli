﻿import { Component, OnInit, OnDestroy, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Subscription, Observable, fromEvent } from 'rxjs';
import { Subject } from 'rxjs';
import { first, map, finalize, takeUntil } from 'rxjs/operators';
import { addDays, startOfMonth, startOfWeek, startOfDay, endOfMonth, endOfWeek, endOfDay, format, isSameMonth, isSameDay, addMinutes } from 'date-fns';

import { CalendarEvent, CalendarEventTimesChangedEvent} from 'angular-calendar';
import { colors } from '@/calendarHeader/colors';

import { User, CalendarItem } from '@/_models';
import { UserService, AuthenticationService, CalendarService } from '@/_services';
import { MatSnackBar, MatDialog } from '@angular/material';

import moment from 'moment';
import { DayViewHourSegment } from 'calendar-utils';

function floorToNearest(amount: number, precision: number) {
    return Math.floor(amount / precision) * precision;
}

function ceilToNearest(amount: number, precision: number) {
    return Math.ceil(amount / precision) * precision;
}

@Component({ 
    selector: 'mwl-demo-component',
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: 'home.component.html',
    styleUrls: ['home.less'] 
})
export class HomeComponent implements OnInit, OnDestroy {
    constructor(
        private authenticationService: AuthenticationService,
        private userService: UserService,
        private calendarService: CalendarService,
        private snackBar: MatSnackBar,
        private cdr: ChangeDetectorRef,
        private dialog: MatDialog
    ) {
        this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
            this.currentUser = user;
        });
    }

    ngOnInit() {
        this.fetchEvents();
    }

    ngOnDestroy() {
        // unsubscribe to ensure no memory leaks
        this.currentUserSubscription.unsubscribe();
    }

    // Variables
    
    currentUser: User;
    currentUserSubscription: Subscription;
    users: User[] = [];
    view: string = 'week';
    viewDate: Date = new Date();
    activeDayIsOpen: boolean = true;
    dragToCreateActive = false;
    checked = false;
    detailview: any = {
        show: false,
        name: null,
        from: null,
        to: null,
        id: null,
        edit: false,
        blocker: false,
    };
    loading: boolean = true;
    calendars = [];
    
    events: CalendarEvent[] = [
        {
            id: "5c7e8213b401f94be0ecf7c5",
            title: 'Draggable event',
            color: colors.yellow,
            start: new Date(),
            end: addMinutes(new Date(), 45),
            draggable: true,
            resizable: {
                beforeStart: true, // this allows you to configure the sides the event is resizable from
                afterEnd: true
            },
        },
        {
            title: 'Resizable event',
            color: colors.yellow,
            start: new Date(),
            end: addDays(new Date(), 1), // an end date is always required for resizable events to work
            resizable: {
                beforeStart: true, // this allows you to configure the sides the event is resizable from
                afterEnd: true
            },
            draggable: true,
            id: "oijwoger"
        }
    ];

    

    debug(){
        console.log(this.events);
        console.log(this.viewDate);
        this.snackBar.open("Test", "Ok", {
            duration: 2000,
        });
    }
    

    refresh: Subject<any> = new Subject();

    eventTimesChanged({event, newStart, newEnd}: CalendarEventTimesChangedEvent): void {
        var oldStart = event.start;
        var oldEnd = event.end;
        event.start = newStart;
        event.end = newEnd;
        this.refresh.next();
        if (new Date(newStart).getTime() !== new Date(oldStart).getTime() || new Date(newEnd).getTime() !== new Date(oldEnd).getTime()){
            var undo = false;
            var saveSnackbar = this.snackBar.open("Wird gespeichert", "Schließen", {
                duration: 5000,
            });
            this.calendarService.updateAppointment(event.id, { from: new Date(event.start).getTime(), to: new Date(event.end).getTime() }).pipe(first()).subscribe(res => {
                saveSnackbar.dismiss();
                // Failed to update, refresh whole calendar view
                console.log(res);
                if (res.status !== 200) {
                    this.events = [];
                    this.fetchEvents();
                }
                else{
                    var undoSnackbar = this.snackBar.open("Gespeichert", "Rückgängig", {
                        duration: 5000,
                    });
                    
                    undoSnackbar.onAction().subscribe(() => {
                        event.start = oldStart;
                        event.end = oldEnd;
                        undo = true;
                        this.refresh.next();
                    });
                    undoSnackbar.afterDismissed().subscribe(() => {
                        if (undo === true) {
                            this.calendarService.updateAppointment(event.id, { from: new Date(oldStart).getTime(), to: new Date(oldEnd).getTime() }).pipe(first()).subscribe(res => {
                                // Failed to update, refresh whole calendar view
                                console.log(res);
                                if (res.status !== 200) {
                                    this.events = [];
                                    this.fetchEvents();
                                }
                            })
                        }
                    });
                }
            })

            
        }
    }

    eventClicked({ event }: { event: CalendarEvent }): void {
        // this.detailview = {
        //     show: true,
        //     name: event.title,
        //     from: moment(event.start).format('dd DD.MM.YYYY HH:mm'),
        //     to: moment(event.end).format('HH:mm'),
        //     id: event.id,
        //     edit: false
        // }

        this.detailview = event;
        this.detailview.show = true;
        this.detailview.edit = false;
        this.detailview.newEvent= false;
        console.log(this.detailview);
    }

    dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
        if (isSameMonth(date, this.viewDate)) {
            this.viewDate = date;
            if (
                (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
                events.length === 0
            ) {
                this.activeDayIsOpen = false;
            } else {
                this.activeDayIsOpen = true;
            }
        }
    }

    saveEditedEvent(): void {
        this.detailview.edit = false;
        
        if(this.detailview.newEvent){
            // this is a new event
            this.calendarService.createAppointment(
                    this.detailview.calendarId, 
                    this.detailview.name, 
                    moment(new Date()).unix() * 1000,
                    moment(addMinutes(new Date(), 45)).unix() * 1000,
                    this.detailview.blocker
                ).pipe(first()).subscribe(res => {
                console.log(res);
            })
        }
        else{
            var update = {

            }
            this.calendarService.updateAppointment(this.detailview.id, update).pipe(first()).subscribe(res => {

            });
        }
    }

    createEvent(): void {
        this.detailview = {
            show: true,
            name: "",
            from: moment(new Date()).format('dd DD.MM.YYYY HH:mm'),
            to: moment(addMinutes(new Date(), 45)).format('HH:mm'),
            id: "",
            edit: true,
            newEvent: true,
            calendarId: "5c7e725e1747cf4298f1b3de_1"
        }
    }

    saveUpdateToDb(id, update){
        
    }

    startDragToCreate(
        segment: DayViewHourSegment,
        mouseDownEvent: MouseEvent,
        segmentElement: HTMLElement
    ) {
        const dragToSelectEvent: CalendarEvent = {
            id: null,
            title: 'New event',
            start: segment.date,
            draggable: true,
            resizable: {
                beforeStart: true, // this allows you to configure the sides the event is resizable from
                afterEnd: true
            },
            meta: {
                tmpEvent: true
            }
        };
        this.events = [...this.events, dragToSelectEvent];
        const segmentPosition = segmentElement.getBoundingClientRect();
        this.dragToCreateActive = true;
        const endOfView = endOfWeek(this.viewDate);

        fromEvent(document, 'mousemove')
            .pipe(
                finalize(() => {
                    delete dragToSelectEvent.meta.tmpEvent;
                    this.dragToCreateActive = false;
                    this.refresh.next();
                }),
                takeUntil(fromEvent(document, 'mouseup'))
            )
            .subscribe((mouseMoveEvent: MouseEvent) => {
                const minutesDiff = ceilToNearest(
                    mouseMoveEvent.clientY - segmentPosition.top,
                    30
                );

                const daysDiff =
                    floorToNearest(
                        mouseMoveEvent.clientX - segmentPosition.left,
                        segmentPosition.width
                    ) / segmentPosition.width;

                const newEnd = addDays(addMinutes(segment.date, minutesDiff), daysDiff);
                if (newEnd > segment.date && newEnd < endOfView) {
                    dragToSelectEvent.end = newEnd;
                }
                this.refresh.next();
            });

            // fromEvent(document, 'mouseup').subscribe((mouseUpEvent: MouseEvent) => {
            //     console.log("MouseUp");
            // })
    }

    fetchEvents(): void {
        const getStart: any = {
            month: startOfMonth,
            week: startOfWeek,
            day: startOfDay
        }[this.view];

        const getEnd: any = {
            month: endOfMonth,
            week: endOfWeek,
            day: endOfDay
        }[this.view];

        this.calendarService.getCalendarItems(moment(getStart(this.viewDate)).unix() * 1000, moment(getEnd(this.viewDate)).unix() * 1000).pipe(first()).subscribe(res => {
            console.log(res);
            var newEvents = [];
            res["calendars"].forEach( (cal) => {
                this.calendars.push({
                    calendarId: cal.calendarId,
                    description: cal.calendarDescription,
                    calendarName: cal.calendarName
                });
                var col = colors.blue;
                cal["items"].forEach((item) => {
                    if(item.cancelled) col = colors.cancelled;
                    var newEvent = {
                        title: item.details.name + " " + item.details.familyname,
                        name: item.details.name,
                        familyname: item.details.familyname,
                        id: item._id,
                        cal: item.calendarId,
                        calName: cal.calendarName,
                        cancelled: item.cancelled,
                        color: col,
                        start: new Date(item.from),
                        end: new Date(item.to), 
                        resizable: {
                            beforeStart: true,
                            afterEnd: true
                        },
                        draggable: true
                    }
                    newEvents.push(newEvent);
                })
            })
            // this.events = [...this.events, ...newEvents];
            this.events = newEvents;
            this.loading = false;
            this.refresh.next();
        });
    }

    deleteEvent(): void{
        this.detailview["edit"] = false;
        this.detailview["show"] = false;
        if (!confirm("Wollen Sie den Termin von " + this.detailview["name"] + " wirklich absagen?")) return;

        var message = prompt("Bitte geben Sie eine Nachricht an " + this.detailview["name"] + " ein.");
        this.calendarService.deleteAppointment(this.detailview["id"], message).pipe(first()).subscribe(res => {
            console.log(res);
            this.fetchEvents();
            this.snackBar.open("Termin erfolgreich abgesagt", "Ok", {
                duration: 5000,
            });

        });
    }

    
}