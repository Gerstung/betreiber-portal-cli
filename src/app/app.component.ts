﻿import { Component } from '@angular/core';
import { Router } from '@angular/router';

import * as moment from 'moment';

import { AuthenticationService } from './_services';
import { User } from './_models';

@Component({ selector: 'app', templateUrl: 'app.component.html', styleUrls: ['./app.component.less'] })
export class AppComponent {
    currentUser: User;

    constructor(
        private router: Router,
        private authenticationService: AuthenticationService
    ) {
        this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
    }

    logout() {
        this.authenticationService.logout(this.currentUser.userId, this.currentUser.providerId, this.currentUser.sessionId);
        this.router.navigate(['/login']);
    }
}