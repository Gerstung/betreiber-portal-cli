﻿// Calendars shows a list of today
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';


import { User } from '@/_models';
import {CalendarItem} from '@/_models';
import { UserService, CalendarService, AuthenticationService } from '@/_services';
import { debug } from 'util';
import * as moment from 'moment';

@Component({ templateUrl: 'calendars.component.html' })
export class CalendarsComponent implements OnInit, OnDestroy {
    currentUser: User;
    currentUserSubscription: Subscription;
    calendarItems: CalendarItem[] = [];
    dateToday: number; 
    loading: boolean;
    edit: boolean;

    constructor(
        private calendarService: CalendarService
    ) {

        this.dateToday = moment().startOf('day').unix() * 1000;
    }

    ngOnInit() {
        this.loading = true;
        this.edit = false;
        this.getItemsForToday();
    }

    ngOnDestroy() {

    }

    private getItemsForToday() {
        var todayStart = moment().startOf('day').unix() * 1000;
        var todayEnd = moment().endOf('day').unix() * 1000;
        this.calendarService.getCalendarItems(todayStart, todayEnd).pipe(first()).subscribe(doc => {
            this.loading = false;
           this.calendarItems = doc;
        });
    }

    deleteCalendar(element: any){
        if (element.items.length === 0){
            if (confirm("Wollen Sie wirklich den Kalender " + element.calendarName + " löschen?")){
                // TODO
                alert("Deleting calendar");
            }
            else{
                alert("abort");
            }
        }
        else{
            if (prompt("In dem Kalender " + element.calendarName + " sind bereits Termine vermerkt. Wenn Sie den Kalender wirklich löschen wollen, tippen sie die Zahl 42 ein.", "") === "42"){
                // TODO
                alert("Deleting calendar");
            }
            else{
                alert("abort");
            }
        }
    }
}