import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CalendarItem } from '@/_models';
import { Config } from '@/_config';

@Injectable({ providedIn: 'root' })
export class CalendarService {
    constructor(
        private http: HttpClient,
        private config: Config
        ) { }

    getCalendarItems(from: number, to: number){
        return this.http.post<CalendarItem[]>(`${this.config.apiUrl}/provider/getCalendarItems`, {
            "from": from, 
            "to": to,
            "cancelled": false
        });
    };

    addCalendar(name: string, description: string, parallel: number){
        return this.http.post<any>(`${this.config.apiUrl}/provider/addCalendar`, {
            "calendar": {
                "name": name, "description": description, "parallelAppointments": parallel}
            });
    };

    createAppointment(calendarId: any, name: any, from: any, to: any, blocker: any){
        var obj:any = {
            "calendarId": calendarId,
            "from": from,
            "to": to,
            "name": name,
            "details": {}
        }
        if (blocker) obj.blocker = true;

        return this.http.post<any>(`${this.config.apiUrl}/provider/createCalendarItem`, obj)
    }

    updateAppointment(appointmentId: any, update: any){
        return this.http.post<any>(`${this.config.apiUrl}/provider/updateCalendarItem`, {
            "calendarItemId": appointmentId,
            "update": update
        })
    }

    deleteAppointment(appointmentId: string, message: string){
        return this.http.post<any>(`${this.config.apiUrl}/provider/deleteCalendarItem`, {
            "calendarItemId": appointmentId,
            "message": message
        });
    };
    
}