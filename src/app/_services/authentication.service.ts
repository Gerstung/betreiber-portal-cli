﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Config } from '@/_config';

import { User } from '@/_models';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;

    constructor(
        private http: HttpClient,
        private config: Config
        ) {
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(sessionStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    login(mail: string, password: string, force: boolean) {
        return this.http.post<any>(`${this.config.apiUrl}/users/login`, {mail, password, force })
            .pipe(map(user => {
                // login successful if there's a jwt sessionId in the response
                if (user && user.sessionId) {
                    // store user details and jwt sessionId in local storage to keep user logged in between page refreshes
                    sessionStorage.setItem('currentUser', JSON.stringify(user));
                    this.currentUserSubject.next(user);
                }

                return user;
            }));
    }

    logout(userId: string, providerId: string, sessionId: string) {
        // log out from backend
        this.http.post<any>(`${this.config.apiUrl}/users/logout`, { userId, providerId, sessionId})
            .subscribe((res: Response) => {
                if (res.status === 200){
                    console.log("successfully logged out");
                }
            })
        // remove user from local storage to log user out
        sessionStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
    }
}