export const colors: any = {
    red: {
        primary: '#ad2121',
        secondary: '#FAE3E3'
    },
    blue: {
        primary: '#1e90ff',
        secondary: '#D1E8FF'
    },
    yellow: {
        primary: '#e3bc08',
        secondary: '#FDF1BA'
    },
    cancelled: {
        primary: '#FFFFFF',
        secondary: '#000000'
    }
};

export function getColorsForIndex(index: Number) {
    var colors = [
        {
            name: "red",
            primary: '#ad2121',
            secondary: '#FAE3E3' 
        },
        {
            name: "blue",
            primary: '#1e90ff',
            secondary: '#D1E8FF'
        },
        {
            name: "yellow",
            primary: '#e3bc08',
            secondary: '#FDF1BA'
        },
        {
            name: "green",
            primary: '#70ff8a',
            secondary: '#FFFFFF'
        }
    ]

    return null;
} 