﻿// Calendars shows a list of today
import { Component, OnInit, OnDestroy } from '@angular/core';
import { first } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


import { CalendarService, AlertService  } from '@/_services';
import { Router } from '@angular/router';

@Component({ templateUrl: 'addCalendar.component.html' })
export class AddCalendarComponent implements OnInit, OnDestroy {
    calendarForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;

    constructor(
        private formBuilder: FormBuilder,
        private alertService: AlertService,
        private calendarService: CalendarService,
        private router: Router
    ) {
    }

    ngOnInit() {
        this.calendarForm = this.formBuilder.group({
            name: ['', Validators.required],
            description: ['', Validators.required],
            parallel: [1, Validators.required],
            from: ['08:00'],
            to: ['17:00']
        });

        // get return url from route parameters or default to '/'
        this.returnUrl = '\calendars';
    }

    ngOnDestroy(){

    }

    // convenience getter for easy access to form fields
    get f() { return this.calendarForm.controls; }

    onSubmit() {
        debugger;
        this.submitted = true;

        // stop here if form is invalid
        if (this.calendarForm.invalid) {
            return;
        }

        this.loading = true;
        this.calendarService.addCalendar(this.f.name.value, this.f.description.value, this.f.parallel.value)
            .pipe(first())
            .subscribe(
                data => {
                    this.router.navigate(['/calendarss']);//[this.returnUrl]);
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }
}