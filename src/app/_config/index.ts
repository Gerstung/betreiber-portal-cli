import { Injectable } from '@angular/core';

@Injectable()
export class Config {
    apiUrl: string = 'http://localhost:3001';
    // apiUrl: string = 'http://on-time-backend.herokuapp.com'
}