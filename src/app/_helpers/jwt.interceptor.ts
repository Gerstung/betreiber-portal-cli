import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';

import { AuthenticationService } from '@/_services';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(private authenticationService: AuthenticationService) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add session ID to body
        let currentUser = this.authenticationService.currentUserValue;

        if (currentUser && currentUser.sessionId) {
            var body = request.body;
            if(body === null) body = {};
            body.sessionId = currentUser.sessionId;
            body.userId = currentUser.userId;
            body.providerId = currentUser.providerId; 
            request = request.clone({
                body: body
            })
        }

        return next.handle(request);
    }
}