export class CalendarItem {
    _id: string;
    calendarId: string;
    from: number;
    to: number;
    length: number;
    created: number;
    type: string;
    confirmed: boolean;
    confirmedTime: number;
    cancelled: boolean;
    cancelledTime: number;
    details: object;
}