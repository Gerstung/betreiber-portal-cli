﻿export class User {
    id: string;
    mail: string;
    password: string;
    firstName: string;
    lastName: string;
    sessionId: string;
    userId: string;
    providerId: string;
}